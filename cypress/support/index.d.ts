declare namespace Cypress {
  interface Chainable<Subject = any> {
    login(): Chainable<Subject>;
    hideIntroModal(): void;
  }
}
