const BASE_URL = Cypress.env('base_url');
const USER_NAME = Cypress.env('username');
const PASSWORD = Cypress.env('password');

function login() {
  cy.visit(`${BASE_URL}/dashboard`);
  return cy.request('POST', `/api/auth/signin`, {
    email: USER_NAME,
    password: PASSWORD,
    totpVerificationCode: null,
  });
  // cy.get('.qa-submitAccept').click();
}

function hideIntroModal() {
  cy.get('.qa-intro-modal .fa-times').click();
}

Cypress.Commands.add('login', login);
Cypress.Commands.add('hideIntroModal', hideIntroModal);
