import './commands';

Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test from "Uncaught TypeError"
  console.warning('uncaught:exception is, ', err);
  return false;
});
