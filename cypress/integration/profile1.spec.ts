describe('Test Profile', () => {
  const BASE_URL = Cypress.env('base_url');
  const USER_NAME = 'test@someemailtest.com';
  const PASSWORD = '1234Test';

  it('Test profile edit', () => {
    cy.visit(`${BASE_URL}/dashboard`);
    cy.get('input')
      .first()
      .type(USER_NAME);
    cy.get('[type="password"]').type(PASSWORD);
    cy.get('[type="submit"]')
    .click()
    .url()
    .should('contain', 'sign-in');

    // repeat many times to test speed
    cy.visit(`${BASE_URL}/dashboard`);
    cy.get('input')
      .first()
      .type(USER_NAME);
    cy.get('[type="password"]').type(PASSWORD);
    cy.get('[type="submit"]')
      .click()
      .url()
      .should('contain', 'sign-in');

    // repeat many times to test speed
    cy.visit(`${BASE_URL}/dashboard`);
    cy.get('input')
      .first()
      .type(USER_NAME);
    cy.get('[type="password"]').type(PASSWORD);
    cy.get('[type="submit"]')
      .click()
      .url()
      .should('contain', 'sign-in');
  });

  it('Test profile edit', () => {
    cy.visit(`${BASE_URL}/dashboard`);
    cy.get('input')
      .first()
      .type(USER_NAME);
    cy.get('[type="password"]').type(PASSWORD);
    cy.get('[type="submit"]')
    .click()
    .url()
    .should('contain', 'sign-in');

    // repeat many times to test speed
    cy.visit(`${BASE_URL}/dashboard`);
    cy.get('input')
      .first()
      .type(USER_NAME);
    cy.get('[type="password"]').type(PASSWORD);
    cy.get('[type="submit"]')
      .click()
      .url()
      .should('contain', 'sign-in');

    // repeat many times to test speed
    cy.visit(`${BASE_URL}/dashboard`);
    cy.get('input')
      .first()
      .type(USER_NAME);
    cy.get('[type="password"]').type(PASSWORD);
    cy.get('[type="submit"]')
      .click()
      .url()
      .should('contain', 'sign-in');
  });
  
  it('Test profile edit', () => {
    cy.visit(`${BASE_URL}/dashboard`);
    cy.get('input')
      .first()
      .type(USER_NAME);
    cy.get('[type="password"]').type(PASSWORD);
    cy.get('[type="submit"]')
    .click()
    .url()
    .should('contain', 'sign-in');

    // repeat many times to test speed
    cy.visit(`${BASE_URL}/dashboard`);
    cy.get('input')
      .first()
      .type(USER_NAME);
    cy.get('[type="password"]').type(PASSWORD);
    cy.get('[type="submit"]')
      .click()
      .url()
      .should('contain', 'sign-in');

    // repeat many times to test speed
    cy.visit(`${BASE_URL}/dashboard`);
    cy.get('input')
      .first()
      .type(USER_NAME);
    cy.get('[type="password"]').type(PASSWORD);
    cy.get('[type="submit"]')
      .click()
      .url()
      .should('contain', 'sign-in');
  });
})
